import pydub
import os

from pydub import AudioSegment
from pydub.audio_segment import extract_wav_headers

files_path = 'genres/full'

startTime = 15 * 1000
endTime = 18 * 1000

for root, dirs, files in os.walk(files_path):
    for file in files:
        s = ''
        for i in file:
            if (i == '.'):
                break
            s+=i
    
        if (file.endswith('.mf')):
            continue

        song = AudioSegment.from_wav(root + '/' + file)
        extract = song[startTime:endTime]
        extract.export('genres/cut/' + s + '/' + file, format = "wav")